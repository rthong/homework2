#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

int main(){
    
    int sum = 0;//initialize sum variable
    vector<int> counter;//create a vector to store numbers in
    
    for(int i=1; i<1001; i++){//running for loop from 1 to 1000
        if(i%3==0 || i%5==0){//Checking if the numbers are multiples of 3 or 5
            counter.push_back(i);
            sum = sum + i;
        }else{
            
        }
    }
    
    cout<< "The natural numbers in the interval are: ";
    for(int i=0; i<counter.size();i++){//Printing multiuples from the interval out
        cout<< counter[i] << " ";
    }
    cout<<endl;
    cout<< "The sum of the natural numbers are: "<< sum<<endl;
    
    return 0;
}
