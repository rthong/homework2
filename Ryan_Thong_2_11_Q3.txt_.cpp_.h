#include <iostream>
#include <cmath>
#include <vector>
#include <random>

using namespace std;

int main(){
    srand(1);//a seed
    vector<float> counter;//vector that will store random numbers
    bool status;// boolean to keep track of loop
    status = true;//initialize variable
    
    while (status == true){//coniditional statement for loop
        float random = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);//changes random number to a float
        
        counter.push_back(random);//pushes random number to array
        cout<<"To continue, press 1. To exit, press 0."<<endl;//prompts user to make a choice to continue
        
        cin>>status;//Checks for User input
    }
    
    cout<<"The random list is: "<<endl;//prints out the list of random numbers in array
    for(int i=0; i < counter.size(); i++){
        cout<<"Entry " << i << ": " << counter[i]<<endl;
    }
    
    return 0;
}

