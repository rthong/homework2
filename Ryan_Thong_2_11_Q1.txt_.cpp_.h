#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

int main(){
    float sum = 0;//Initialize sum
    float average;
    float standard_deviation = 0;//Initialize Standadard Deviation
    
    vector<float> Q;
    Q.push_back(2.3);
    Q.push_back(4.7);
    Q.push_back(0.1);
    Q.push_back(6.4);
    Q.push_back(3.8);
    Q.push_back(9.6);
    Q.push_back(9.0);
    Q.push_back(6.1);
    Q.push_back(7.3);
    Q.push_back(2.2);
    
    for( int i = 0; i<Q.size(); i++){//Creates a running sum
        sum = sum + Q[i];
    }
    
    average = sum/Q.size();//Divide total sum by total elements
    
    for(int i = 0; i<Q.size(); i++){//running sum of inner standard deviation
        standard_deviation = standard_deviation + pow(Q[i] - average, 2);
    }
    standard_deviation = pow(standard_deviation/Q.size(),1/2);//calculates standard deviation
    
    cout<< "The sum is: " << sum <<endl;
    cout<< "Average : " << average <<endl;
    cout<< "Standard Deviation : " << standard_deviation <<endl;
    
    return 0;
}
