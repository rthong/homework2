#include <iostream>
#include <vector>
#include <string>
#include <cmath>

using namespace std;

int main(){//Determining a palindrome of max value for two three-digit numbers
    vector<int> palindrome;//vector to hold all instances of palindromes
    bool status;//boolean
    status = true;//initialization
    
    for(int i = 100; i < 1001; i++ ) {//running a for loop from 100 to 1000
        for (int j = i; j < 1001; j++) {//running a for loop from i to 1000, to avoid any repetition
            string product = to_string(i*j);//making the product a string to compare each element
            status = true;//resetting the boolean
            
            for(int z = 0; z < ceil(product.size()/2); z++){//Checking elements from out to in
                if(product.at(z) != product.at(product.size() - z - 1)){//Conditional statement checking if elements are not equivalent
                    status = false;
                }
                
            }
            if (status == true){//will store palindrome in palindrome vector
                palindrome.push_back(i*j);
            }
            
        }
    }
    
    int max = palindrome[0];//initialize max value to compare
    
    for(int i = 0; i< (palindrome.size()-1); i++){//running through palindrome array to determine true max
        if(  max >= palindrome[i+1] ){
        }else{
            max = palindrome[i+1];
        }
    }
    
    cout <<"Max number is: " << max << endl;
    return 0;
}
